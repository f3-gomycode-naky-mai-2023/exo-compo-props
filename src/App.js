import logo from './logo.svg';
import './App.css';
import Name from './components/Name';
import Price from './components/Price';
import Description from './components/Description';
import Image from './components/Image';
import Card from 'react-bootstrap/Card';
import PlayersList from './components/PlayersList';
function App() {
  return (
    <div className="App">
      {/* Hello, M.Nacky
       <Card style={{ width: '18rem' }}>
          <Image></Image>
          <Card.Body>
            <Card.Title><Name></Name></Card.Title>
            <Card.Text>
            <Price></Price>
            <Description></Description>
            </Card.Text>
          </Card.Body>
        </Card> */}

        <PlayersList></PlayersList>
    </div>

  );
}

export default App;
