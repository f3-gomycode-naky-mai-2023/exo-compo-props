import React from 'react';
import players from '../players';
import Player from './Player';

const PlayersList = () => {
    return (
        <div style={{display: 'flex'}}>
         {players && players.map((player, key) => (<Player key={key} player={player} />))}   
        </div>
    );
}

export default PlayersList;
