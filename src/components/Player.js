import React from 'react';
import Card from 'react-bootstrap/Card';

const Player = (props) => {
    const {name = "Kanouté", nationalite="Malienne", equipe="Mali", numMaillot=12, age=23, image="/images/mais.jpeg"} = props
    return (
        <div>
            <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={image} />
                <Card.Body>
                    <Card.Title>Nom: {name}</Card.Title>
                    <Card.Text>
                        Nationalité: {nationalite}
                    </Card.Text>
                    <Card.Text>
                        Equipe:  {equipe}
                    </Card.Text>
                    <Card.Text>
                        Maillot: {numMaillot}
                    </Card.Text>
                    <Card.Text>
                        Age: {age}
                    </Card.Text>
                </Card.Body>
            </Card>
        </div>
    );
}

export default Player;
