import React from 'react';
import product from '../product';

const Price = () => {
    return (
        <div>
            Price: {product.price} XOF
        </div>
    );
}

export default Price;
