const players = [
    {
        name: "Kanouté",
        nationalite: "Malien",
        equipe: "Mali",
        numMaillot: 12,
        age: 20,
        image: "/images/mais.jpeg"
    },
    {
        name: "Keita",
        nationalite: "Ivoirien",
        equipe: "Côte d'Ivoire",
        numMaillot: 21,
        age: 23,
        image: "/images/mais.jpeg"
    },
    {
        name: "Messi",
        nationalite: "Argentin",
        equipe: "Argentine",
        numMaillot: 10,
        age: 24,
        image: "/images/mais.jpeg"
    },
    {
        name: "Cristinano",
        nationalite: "Portugais",
        equipe: "Portugal",
        numMaillot: 7,
        age: 30,
        image: "/images/mais.jpeg"
    }
]

export default players;